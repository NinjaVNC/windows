# Introduction
NinjaVNC is a Remote Access Tool which gives you the ability to control, 
monitor and record another desktop. It is built on top of TightVNC and 
extends it with surveillance capabilities to remotely record screenshots, 
keystrokes and mouse clicks.

# Building
The code based is setup to be built by gradle using
```
msbuild.exe WinVNC.sln /p:Configuration=Release /p:Platform=x86
```
Please see details in winvnc\build.gradle and .gitlab-ci.yml

# Running
The basic way of running the NinjaVNC client is by running the WinVNC.exe executable.

# Credits and license
NinjaVNC stands on the shoulders of giants and a great thank you to all the
hard work that has gone into this codebase over the years.

Copyright (C) 2022 NinjaVNC Johan Lundin.  All Rights Reserved.

Copyright (C) 2004 Horizon Wimba.  All Rights Reserved.

Copyright (C) 2002-2005 RealVNC Ltd.  All Rights Reserved.

Copyright (C) 2001-2004 HorizonLive.com, Inc.  All Rights Reserved.

Copyright (C) 2002-2006 Constantin Kaplinsky.  All Rights Reserved.

Copyright (C) 2002 Cendio Systems.  All Rights Reserved.

Copyright (C) 2000 Tridia Corporation.  All Rights Reserved.

Copyright (C) 1999 AT&T Laboratories Cambridge.  All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This software is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this software; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
USA.
