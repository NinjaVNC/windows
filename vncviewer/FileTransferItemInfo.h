//
//  Copyright (C) 2022 NinjaVNC Johan Lundin.  All Rights Reserved.
//  Copyright (C) 2004 Horizon Wimba.  All Rights Reserved.
//  Copyright (C) 2002-2005 RealVNC Ltd.  All Rights Reserved.
//  Copyright (C) 2001-2004 HorizonLive.com, Inc.  All Rights Reserved.
//  Copyright (C) 2002-2006 Constantin Kaplinsky.  All Rights Reserved.
//  Copyright (C) 2002 Cendio Systems.  All Rights Reserved.
//  Copyright (C) 2000 Tridia Corporation.  All Rights Reserved.
//  Copyright (C) 1999 AT&T Laboratories Cambridge.  All Rights Reserved.
//
//  This is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This software is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this software; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
//  USA.
//

#if !defined(FILETRANSFERITEMINFO_H)
#define FILETRANSFERITEMINFO_H

#define rfbMAX_PATH 255

typedef struct tagFTITEMINFO
{
    char Name[rfbMAX_PATH];
    char Size[16];
	unsigned int Data;
} FTITEMINFO;

typedef struct tagFTSIZEDATA
{
	unsigned int size;
	unsigned int data;
} FTSIZEDATA;

class FileTransferItemInfo  
{
public:
	int GetIntSizeAt(int Number);
	static const char folderText[];
	int GetNumEntries();
	char * GetSizeAt(int Number);
	char * GetNameAt(int Number);
	unsigned int GetDataAt(int Number);
    bool IsFile(int Number);    
	void Sort();
	void Free();
	void Add(char *Name, char *Size, unsigned int Data);
	FileTransferItemInfo();
	virtual ~FileTransferItemInfo();

private:
	int ConvertCharToInt(char *pStr);
	FTITEMINFO * m_pEntries;
	int m_NumEntries;
};

#endif // !defined(FILETRANSFERITEMINFO_H)
