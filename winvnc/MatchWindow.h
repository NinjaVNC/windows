//
//  Copyright (C) 2022 NinjaVNC Johan Lundin.  All Rights Reserved.
//  Copyright (C) 2004 Horizon Wimba.  All Rights Reserved.
//  Copyright (C) 2002-2005 RealVNC Ltd.  All Rights Reserved.
//  Copyright (C) 2001-2004 HorizonLive.com, Inc.  All Rights Reserved.
//  Copyright (C) 2002-2006 Constantin Kaplinsky.  All Rights Reserved.
//  Copyright (C) 2002 Cendio Systems.  All Rights Reserved.
//  Copyright (C) 2000 Tridia Corporation.  All Rights Reserved.
//  Copyright (C) 1999 AT&T Laboratories Cambridge.  All Rights Reserved.
//
//  This is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This software is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this software; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
//  USA.
//


// MatchWindow.h: interface for the CMatchWindow class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MATCHWINDOW_H__11975916_3DA2_11D3_8EA3_008048C6AFB8__INCLUDED_)
#define AFX_MATCHWINDOW_H__11975916_3DA2_11D3_8EA3_008048C6AFB8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class vncServer;

class CMatchWindow  
{
public:
	BOOL ModifyPosition(int left,int top, int right,int bottom);
	CMatchWindow(vncServer* pDlg,int left,int top, int right,int bottom);
	virtual ~CMatchWindow();
	void GetPosition(int &left,int &top, int &right,int &bottom);
	HWND m_hWnd;
	BOOL m_bSized;
	POINT m_TrackerHits[8];
	int HitTest(POINT&); // -1 = no Hits; 0-7 Numb. of Hit
    void ArrangeHits();
    void ChangeRegion();
	void Show();
	void Hide();
	void CanModify(BOOL);
	static LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
    vncServer* m_pServer;
};

#endif // !defined(AFX_MATCHWINDOW_H__11975916_3DA2_11D3_8EA3_008048C6AFB8__INCLUDED_)
