//
//  Copyright (C) 2022 NinjaVNC Johan Lundin.  All Rights Reserved.
//  Copyright (C) 2004 Horizon Wimba.  All Rights Reserved.
//  Copyright (C) 2002-2005 RealVNC Ltd.  All Rights Reserved.
//  Copyright (C) 2001-2004 HorizonLive.com, Inc.  All Rights Reserved.
//  Copyright (C) 2002-2006 Constantin Kaplinsky.  All Rights Reserved.
//  Copyright (C) 2002 Cendio Systems.  All Rights Reserved.
//  Copyright (C) 2000 Tridia Corporation.  All Rights Reserved.
//  Copyright (C) 1999 AT&T Laboratories Cambridge.  All Rights Reserved.
//
//  This is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This software is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this software; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
//  USA.
//

#ifndef __WALLPAPERUTILS_H
#define __WALLPAPERUTILS_H

#include "stdhdrs.h"

#include <wininet.h>
#include <shlobj.h>

#include "WinVNC.h"
#include "Log.h"

class WallpaperUtils
{
public:
	WallpaperUtils();

	void KillWallpaper();
	void RestoreWallpaper();

protected:
	// NOTE: Before using any of the following two functions, the caller MUST
	//       initialize the COM library, e.g. by calling CoInitialize(NULL).
	void KillActiveDesktop();
	void RestoreActiveDesktop();

	bool m_restore_ActiveDesktop;
	bool m_restore_wallpaper;
};

#endif // __WALLPAPERUTILS_H
