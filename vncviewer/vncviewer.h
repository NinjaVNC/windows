//
//  Copyright (C) 2022 NinjaVNC Johan Lundin.  All Rights Reserved.
//  Copyright (C) 2004 Horizon Wimba.  All Rights Reserved.
//  Copyright (C) 2002-2005 RealVNC Ltd.  All Rights Reserved.
//  Copyright (C) 2001-2004 HorizonLive.com, Inc.  All Rights Reserved.
//  Copyright (C) 2002-2006 Constantin Kaplinsky.  All Rights Reserved.
//  Copyright (C) 2002 Cendio Systems.  All Rights Reserved.
//  Copyright (C) 2000 Tridia Corporation.  All Rights Reserved.
//  Copyright (C) 1999 AT&T Laboratories Cambridge.  All Rights Reserved.
//
//  This is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This software is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this software; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
//  USA.
//


#ifndef VNCVIEWER_H__
#define VNCVIEWER_H__

#pragma once

#include "res/resource.h"
#include "VNCviewerApp.h"
#include "Log.h"
#include "VNCHelp.h"
#include "HotKeys.h"

#define WM_SOCKEVENT WM_USER+1
#define WM_TRAYNOTIFY WM_SOCKEVENT+1
#define WM_REGIONUPDATED WM_TRAYNOTIFY+1

// The Application
extern VNCviewerApp *pApp;

// Global logger - may be used by anything
extern Log vnclog;
extern VNCHelp help;
extern HotKeys hotkeys;

// Display given window in centre of screen
void CentreWindow(HWND hwnd);

// Convert "host:display" into host and port
// Returns true if valid.
bool ParseDisplay(LPTSTR display, LPTSTR phost, int hostlen, int *port);
void FormatDisplay(int port, LPTSTR display, LPTSTR host);


// Macro DIALOG_MAKEINTRESOURCE is used to allow both normal windows dialogs
// and the selectable aspect ratio dialogs under WinCE (PalmPC vs HPC).
#ifndef UNDER_CE
#define DIALOG_MAKEINTRESOURCE MAKEINTRESOURCE
#else
// Under CE we pick dialog resource according to the 
// screen format selected or determined.
#define DIALOG_MAKEINTRESOURCE(res) SELECT_MAKEINTRESOURCE(res ## _PALM, res)
inline LPTSTR SELECT_MAKEINTRESOURCE(WORD res_palm, WORD res_hpc)
{
	if (pApp->m_options.m_palmpc)
		return MAKEINTRESOURCE(res_palm);
	else
		return MAKEINTRESOURCE(res_hpc);
}
#endif

#endif // VNCVIEWER_H__

