//
//  Copyright (C) 2022 NinjaVNC Johan Lundin.  All Rights Reserved.
//  Copyright (C) 2004 Horizon Wimba.  All Rights Reserved.
//  Copyright (C) 2002-2005 RealVNC Ltd.  All Rights Reserved.
//  Copyright (C) 2001-2004 HorizonLive.com, Inc.  All Rights Reserved.
//  Copyright (C) 2002-2006 Constantin Kaplinsky.  All Rights Reserved.
//  Copyright (C) 2002 Cendio Systems.  All Rights Reserved.
//  Copyright (C) 2000 Tridia Corporation.  All Rights Reserved.
//  Copyright (C) 1999 AT&T Laboratories Cambridge.  All Rights Reserved.
//
//  This is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This software is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this software; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
//  USA.
//


// vncCorbaConnect

// The vncCorbaConnect object makes the WinVNC server available through
// the CORBA desktop control interface

class vncCorbaConnect;

#if (!defined(_WINVNC_VNCCORBACONNECT))
#define _WINVNC_VNCCORBACONNECT

// Includes
#include "stdhdrs.h"

// The following class definition is only used if CORBA control
// is to be enabled in the final executable
#if(defined(_CORBA))

#include <omniorb2/CORBA.h>
#include <omnithread.h>
#include "vnc.hh"
#include "vncServer.h"

// The vncCorbaConnect class itself
class vncCorbaConnect
{
public:
	// Constructor/destructor
	vncCorbaConnect();
	~vncCorbaConnect();

	// Init
	virtual BOOL Init(vncServer *server);

	// Implementation
protected:
	// Internal methods
	virtual BOOL InitQuiet(vncServer *server);
	virtual BOOL InitCorba(int argc, char *argv[]);
	virtual void ShutdownCorba(void);
	virtual CORBA::Boolean BindDesktop(CORBA::Object_ptr obj);

	// General
	vncServer			*m_server;

	// The actual CORBA stuff;
	CORBA::ORB_ptr		m_orb;						// The overall ORB object
	CORBA::BOA_ptr		m_boa;

	vnc::_sk_controller	*m_controller;

	char				*m_username;
	char				*m_desktop;

	CORBA::ULong		m_lastconntime;

	omni_mutex			m_updateLock;
	
	UINT				m_port;
};

#else // _CORBA

#include "vncServer.h"

// The vncCorbaConnect class itself

class vncCorbaConnect
{
public:
	// Init
	virtual BOOL Init(vncServer *server) {return FALSE;};
};

#endif // _CORBA

#endif // _WINVNC_VNCCORBACONNECT
