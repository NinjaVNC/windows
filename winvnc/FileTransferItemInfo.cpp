//
//  Copyright (C) 2022 NinjaVNC Johan Lundin.  All Rights Reserved.
//  Copyright (C) 2004 Horizon Wimba.  All Rights Reserved.
//  Copyright (C) 2002-2005 RealVNC Ltd.  All Rights Reserved.
//  Copyright (C) 2001-2004 HorizonLive.com, Inc.  All Rights Reserved.
//  Copyright (C) 2002-2006 Constantin Kaplinsky.  All Rights Reserved.
//  Copyright (C) 2002 Cendio Systems.  All Rights Reserved.
//  Copyright (C) 2000 Tridia Corporation.  All Rights Reserved.
//  Copyright (C) 1999 AT&T Laboratories Cambridge.  All Rights Reserved.
//
//  This is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This software is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this software; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
//  USA.
//

#include "FileTransferItemInfo.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

FileTransferItemInfo::FileTransferItemInfo()
{
	m_NumEntries = 0;
	m_pEntries = NULL;
}

FileTransferItemInfo::~FileTransferItemInfo()
{
	Free();
}

void FileTransferItemInfo::Add(char *Name, unsigned int Size, unsigned int Data)
{
	FTITEMINFO *pTemporary = new FTITEMINFO[m_NumEntries + 1];
	if (m_NumEntries != 0) 
		memcpy(pTemporary, m_pEntries, m_NumEntries * sizeof(FTITEMINFO));
	strcpy(pTemporary[m_NumEntries].Name, Name);
	pTemporary[m_NumEntries].Size = Size;
	pTemporary[m_NumEntries].Data = Data;
	if (m_pEntries != NULL) {
		delete [] m_pEntries;
		m_pEntries = NULL;
	}
	m_pEntries = pTemporary;
	pTemporary = NULL;
	m_NumEntries++;
}

void FileTransferItemInfo::Free()
{
	if (m_pEntries != NULL) {
		delete [] m_pEntries;
		m_pEntries = NULL;
	}
	m_NumEntries = 0;
}

char * FileTransferItemInfo::GetNameAt(int Number)
{
	if ((Number >= 0) && (Number <= m_NumEntries))
		return m_pEntries[Number].Name;
	return NULL;
}

unsigned int FileTransferItemInfo::GetSizeAt(int Number)
{
	if ((Number >= 0) && (Number <= m_NumEntries)) 
		return m_pEntries[Number].Size; 
	return NULL;
}

unsigned int FileTransferItemInfo::GetDataAt(int Number)
{
	if ((Number >= 0) && (Number <= m_NumEntries)) 
		return m_pEntries[Number].Data; 
	return NULL;
}

int FileTransferItemInfo::GetNumEntries()
{
	return m_NumEntries;
}

int FileTransferItemInfo::GetSummaryNamesLength()
{
	int sumLen = 0;
	for (int i = 0; i < m_NumEntries; i++)
		sumLen += strlen(m_pEntries[i].Name);
	return sumLen;
}
