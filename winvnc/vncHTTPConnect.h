//
//  Copyright (C) 2022 NinjaVNC Johan Lundin.  All Rights Reserved.
//  Copyright (C) 2004 Horizon Wimba.  All Rights Reserved.
//  Copyright (C) 2002-2005 RealVNC Ltd.  All Rights Reserved.
//  Copyright (C) 2001-2004 HorizonLive.com, Inc.  All Rights Reserved.
//  Copyright (C) 2002-2006 Constantin Kaplinsky.  All Rights Reserved.
//  Copyright (C) 2002 Cendio Systems.  All Rights Reserved.
//  Copyright (C) 2000 Tridia Corporation.  All Rights Reserved.
//  Copyright (C) 1999 AT&T Laboratories Cambridge.  All Rights Reserved.
//
//  This is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This software is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this software; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
//  USA.
//


// vncHTTPConnect.h

// The vncHTTPConnect class creates a listening socket and binds
// it to the specified port number.  It then creates a listen
// thread which goes into a loop, listening on the socket.
// When the vncHTTPConnect object is destroyed, all resources are
// freed automatically, including the listen thread.
// This server allows clients to request the java classes required
// to view the desktop remotely.

class vncHTTPConnect;

#if (!defined(_WINVNC_VNCHTTPCONNECT))
#define _WINVNC_VNCHTTPCONNECT

// Includes
#include "stdhdrs.h"
#include "VSocket.h"
#include "vncServer.h"
#include <omnithread.h>

// The vncHTTPConnect class itself
class vncHTTPConnect
{
public:
	// Constructor/destructor
	vncHTTPConnect();
	~vncHTTPConnect();

	// Init
	virtual VBool Init(vncServer *server, UINT listen_port, BOOL allow_params);

	// Implementation
protected:
	// The listening socket
	VSocket m_listen_socket;

	// The port to listen on
	UINT m_listen_port;

	// The in-coming accept thread
	omni_thread *m_listen_thread;

	// Allow passing applet parameters in the URL
	BOOL m_allow_params;
};

#endif // _WINVNC_VNCHTTPCONNECT
