//
//  Copyright (C) 2022 NinjaVNC Johan Lundin.  All Rights Reserved.
//  Copyright (C) 2004 Horizon Wimba.  All Rights Reserved.
//  Copyright (C) 2002-2005 RealVNC Ltd.  All Rights Reserved.
//  Copyright (C) 2001-2004 HorizonLive.com, Inc.  All Rights Reserved.
//  Copyright (C) 2002-2006 Constantin Kaplinsky.  All Rights Reserved.
//  Copyright (C) 2002 Cendio Systems.  All Rights Reserved.
//  Copyright (C) 2000 Tridia Corporation.  All Rights Reserved.
//  Copyright (C) 1999 AT&T Laboratories Cambridge.  All Rights Reserved.
//
//  This is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This software is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this software; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
//  USA.
//


// vncEncodeCoRRE object

// The vncEncodeCoRRE object uses a compression encoding to send rectangles
// to a client

class vncEncodeCoRRE;

#if !defined(_WINVNC_ENCODECORRRE)
#define _WINVNC_ENCODECORRE
#pragma once

#include "vncEncoder.h"

// Class definition

class vncEncodeCoRRE : public vncEncoder
{
// Fields
public:

// Methods
public:
	// Create/Destroy methods
	vncEncodeCoRRE();
	~vncEncodeCoRRE();

	virtual void Init();
	virtual const char* GetEncodingName() { return "CoRRE"; }

	virtual UINT RequiredBuffSize(UINT width, UINT height);
	virtual UINT NumCodedRects(RECT &rect);

	virtual UINT EncodeRect(BYTE *source, BYTE *dest, const RECT &rect, int offx, int offy);
	virtual void SetCoRREMax(BYTE width, BYTE height);
protected:
	virtual UINT InternalEncodeRect(BYTE *source, BYTE *dest, const RECT &rect);
	virtual UINT EncodeSmallRect(BYTE *source, BYTE *dest, const RECT &rect);

// Implementation
protected:
	BYTE		*m_buffer;
	size_t		m_bufflen;

	// Maximum height & width for CoRRE
	int			m_maxwidth;
	int			m_maxheight;

	// Last-update stats for CoRRE
	UINT		m_encodedbytes, m_rectbytes;
	UINT		m_lastencodedbytes, m_lastrectbytes;
	int			m_maxadjust;
	int			m_threshold;
	BOOL		m_statsready;
	int			offsetx;
	int			offsety;
};

#endif // _WINVNC_ENCODECORRE

